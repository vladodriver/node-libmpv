#include "include/headers.hpp"

namespace libmpv {

  //public
  Out::Out() {}
  Out::Out(char* str) : indata(static_cast<void*>(&str)), fmt(MPV_FORMAT_STRING),
  rval(voidDataToValue(static_cast<void*>(indata), fmt)) {}
  Out::Out(void* data, mpv_format format) : indata(data), fmt(format),
  rval(voidDataToValue(indata, fmt)) {}
  Out::Out(mpv_node* node) : indata(static_cast<void*>(node)), fmt(node->format),
  rval(parseMpvNode(node)) {}

  Out::~Out() {}

  void* Out::getDataFromNode(mpv_node *node) {
    void * data = nullptr;
    if(node->format == MPV_FORMAT_STRING || node->format == MPV_FORMAT_OSD_STRING) {
      data = &node->u.string;
    } else if (node->format == MPV_FORMAT_FLAG) {
      data = &node->u.flag;
    } else if (node->format == MPV_FORMAT_INT64) {
      data = &node->u.int64;
    } else if (node->format == MPV_FORMAT_DOUBLE) {
      data = &node->u.double_;
    } else if (node->format == MPV_FORMAT_NODE_ARRAY || node->format == MPV_FORMAT_NODE_MAP) {
      data = &node->u.list;
    } else if (node->format == MPV_FORMAT_BYTE_ARRAY) {
      data = &node->u.ba;
    }
    return data;
  }

  Local<Value> Out::value() { return rval; }

  //private:
  Local<Value> Out::parseMpvNodeList(mpv_node_list *node_list) {
    Local<Value> js_node_list;

    //JS Object or Array -> ismap == true node_map -> Object || false -> Array
    bool ismap = (node_list->keys != nullptr); //
    if (ismap) {
      js_node_list = Nan::New<Object>();
    } else {
      js_node_list = Nan::New<Array>();
    }
    Local<Value> js_node; //actual parsed node

    for(int i = 0; i < node_list->num; i++) {
      js_node = parseMpvNode(&node_list->values[i]); //parse one node

      if(ismap) {
        js_node_list->ToObject()->Set(
          Nan::New<String>(node_list->keys[i]).ToLocalChecked(),
          js_node->ToObject()->Get(Nan::New<String>("data").ToLocalChecked())
        );
      } else {
        js_node_list->ToObject()->Set(
          Nan::New<Integer>(i),
          js_node->ToObject()->Get(Nan::New<String>("data").ToLocalChecked())
        );
      }
    }
    return js_node_list;
  }

  Local<Value> Out::parseMpvNode(mpv_node* node) {
    if(node) {
      Local<Value> js_node = Nan::New<Object>();
      Local<Value> jsdata;
      Fmt<mpv_format> fmt(Maps::for_MAP, node->format);
      js_node->ToObject()->Set(
        Nan::New<String>("type").ToLocalChecked(),
        Nan::New<String>("node").ToLocalChecked()
      );
      js_node->ToObject()->Set(
        Nan::New<String>("format").ToLocalChecked(),
        Nan::New<String>(fmt.cstr()).ToLocalChecked()
      );

      //NONE, STRING, OSD_STRING, FLAG, INT64, DOUBLE
      if (node->format >= 0 && node->format < 7) {
        jsdata = voidDataToValue(getDataFromNode(node), node->format);
        js_node->ToObject()->Set(
          Nan::New<String>("data").ToLocalChecked(),
          jsdata->ToObject()->Get(Nan::New<String>("data").ToLocalChecked())
        );
        //NODE_ARRAY, NODE_MAP -> js objects
      } else if (node->format == 7 || node->format == 8) {
        jsdata = parseMpvNodeList(node->u.list);
        js_node->ToObject()->Set(Nan::New<String>("data").ToLocalChecked(), jsdata);
        //BYTE_ARRAY -> nodejs Buffer object
      } else if (node->format == 9) {
        Local<Object> bf;
        size_t size = node->u.ba->size;
        char* data = (char*)node->u.ba->data;
        bf = Nan::NewBuffer(data, size).ToLocalChecked();
        js_node->ToObject()->Set(Nan::New<String>("data").ToLocalChecked(), bf);
      }
      return js_node;
    } else {
      return Nan::Undefined();
    }
  }

  Local<Value> Out::voidDataToValue(void* data, mpv_format format) {
    Local<Value> ret = Nan::New<Object>();
    Fmt<mpv_format> fmt(Maps::for_MAP, format);
    ret->ToObject()->Set(
      Nan::New<String>("type").ToLocalChecked(),
      Nan::New<String>(fmt.cstr()).ToLocalChecked()
    );

    if(format == MPV_FORMAT_NONE || data == nullptr) {
      ret->ToObject()->Set(Nan::New<String>("data").ToLocalChecked(), Nan::Undefined());
    } else if (format == MPV_FORMAT_STRING || format == MPV_FORMAT_OSD_STRING) {
      ret->ToObject()->Set(
        Nan::New<String>("data").ToLocalChecked(),
        Nan::New<String>(*static_cast<const char**>(data)).ToLocalChecked()
      );
    } else if (format == MPV_FORMAT_FLAG) {
      ret->ToObject()->Set(
        Nan::New<String>("data").ToLocalChecked(),
        Nan::New<Boolean>(*static_cast<bool *>(data))
      );
    } else if (format == MPV_FORMAT_INT64) {
      ret->ToObject()->Set(
        Nan::New<String>("data").ToLocalChecked(),
        Nan::New<Integer>(*static_cast<int32_t *>(data))
      );
    } else if (format == MPV_FORMAT_DOUBLE) {
      ret->ToObject()->Set(
        Nan::New<String>("data").ToLocalChecked(),
        Nan::New<Number>(*static_cast<double *>(data))
      );
    }
    return ret;
  }

}