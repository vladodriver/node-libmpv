#include "include/headers.hpp"

namespace libmpv {

  Core::Core() {}
  Core::~Core() {}

  //public api cmds wrappers
  Local<Value> Core::create(const Nan::FunctionCallbackInfo<Value>& args) {
    mpv_handle* ctx = get_core();
    if(!ctx) {
      main_ctx = mpv_create();
      main_name = string(mpv_client_name(main_ctx));
      clients[main_name] = main_ctx;
      Mpv::log << "MAIN mpv_handle* CTX : " << main_name << " CREATED" << endl;
    }
    return status();
  }

  Local<Value> Core::initialize(const Nan::FunctionCallbackInfo<Value>& args) {
    Result result;
    if (main_ctx == nullptr) { //Ctx not created
      result.addError(-3);
    } else if (core_status == -3) { //Ctx created and not initialized
      core_status = mpv_initialize(clients[main_name]);
      result.addError(core_status);
    } else { //multiple initializations not allowed -> return only no-error status
      result.addError(0);
    }

    result.addData(status());
    return result.getRes();
  }

  Local<Value> Core::createClient(const Nan::FunctionCallbackInfo<Value>& args) {
    Result res;
    In ctx_name(args[0]);
    mpv_handle* core_ctx = get_core();
    mpv_handle* clctx = nullptr;
    
    clctx = mpv_create_client(core_ctx, ctx_name.toStr()); //create clctx based on core ctx
    if(core_ctx) { //crete client based on core named by args[0] string
      In ctx_name(args[0]);
      if(main_ctx && core_status == 0) { //create only if core ctx is initialized
        string cl_name = mpv_client_name(clctx);
        clients[cl_name] = clctx; //add new client
        res.addError(0);
        Mpv::log << "CLIENT mpv_handle* CTX : " << cl_name << " CREATED" << endl;
      } else {
        res.addError(-3);
      }
    } else {
      if(clctx) { //ctx created OK
        //create core ctx only if not exist
        if (!core_ctx) {
          main_ctx = clctx;
          main_name = string(mpv_client_name(clctx));
          clients[main_name] = clctx; //add new client
          Mpv::log << "MAIN mpv_handle* CTX : " << main_name << " CREATED and name param: "
            << ctx_name.toStr() << " ARE IGNORED" << endl;
        } else {
          clctx = nullptr; //reset if core exists
        }
        res.addError(0);
      } else { //memory error clctx is NULL
        res.addError(-2);
      }
    }
    res.addData(status());
    return res.getRes();
  }

  Local<Value> Core::destroyHandle(const Nan::FunctionCallbackInfo<Value>& args, string mode) {
    //pointer mpv_detach.. or mpv_terminate..
    void(*dtfn)(mpv_handle *ctx) = nullptr;

    //set mode dtfn pointer
    if(mode == "detach") {
      dtfn = mpv_detach_destroy;
    } else if (mode == "terminate") {
      dtfn = mpv_terminate_destroy;
    }

    In ctx_name(args[0]);
    //in "terminate" mode OR if cname is main_ctx_name in detach mode -> destroy all clients
    if (clients.find(string(ctx_name.toStr())) != clients.end()) {
      if(mode == "terminate" || ctx_name.toStr() == main_name) {
        //destruct all other client nodes in terminate or detach main ctx prevent deadlock
        map<string, mpv_handle*>::iterator it;
        for (it=clients.begin(); it != clients.end(); ++it ) {
          if(it->first != main_name) {
            Mpv::log << "FORCE DESTROY CLIENT: " << it->first << " -> detachDestroy("
              << main_name << ") or terminateDestroy(<any ctx name>)" << endl;
            dtfn(it->second);
            clients.erase(it);
          }
        }
        //destroy core ctx
        Mpv::log << "DESTROY MAIN CTX: " << main_name << endl;
        dtfn(clients[main_name]);
        //destroy core ctx
        core_status = -3;
        main_ctx = nullptr;
        clients.clear();
      } else if(mode == "detach" && ctx_name.toStr() != main_name) {
        Mpv::log << "DETACH DESTROY CLIENT: " << ctx_name.toStr() << endl;
        dtfn(clients[string(ctx_name.toStr())]); //detach or destroy
        clients.erase(string(ctx_name.toStr())); //erase from clients map
      }
    }
    return status();
  }

  //public methods
  Local<Value> Core::status() {
    Local<Object> sobj = Nan::New<Object>();

    sobj->Set(Nan::New<String>("coreCreated").ToLocalChecked(), Nan::New<Boolean>(main_ctx != nullptr));
    sobj->Set(Nan::New<String>("coreInitialized").ToLocalChecked(), Nan::New<Boolean>(core_status == 0));

    if(main_ctx != nullptr) {
      sobj->Set(Nan::New<String>("coreSuspended").ToLocalChecked(), Nan::New<Boolean>(core_suspended));
      sobj->Set(Nan::New<String>("coreClientName").ToLocalChecked(), 
        Nan::New<String>(main_name.c_str()).ToLocalChecked());
      Local<Object> clob = Nan::New<Object>();
      sobj->Set(Nan::New<String>("clients").ToLocalChecked(), clob);
      //get pointer address as string
      int i; map<string, mpv_handle*>::iterator it;
      for(it=clients.begin(), i = 0; it!=clients.end(); it++, i++) {
        stringstream ss;
        ss << it->second;
        string name(ss.str());
        clob->Set(Nan::New<String>(it->first.c_str()).ToLocalChecked(), Nan::New<String>(name.c_str()).ToLocalChecked());
      }
    }
    return sobj;
  }
  
  //TODO Core::getMaps -> for mpv_enums_getter
  Local<Value> Core::getMaps() {
    Local<Object> mobj = Nan::New<Object>(); //object for all string->enum maps
    
    mobj->Set(Nan::New<String>("format").ToLocalChecked(), Maps::getMap(Maps::for_MAP));
    mobj->Set(Nan::New<String>("end_of_file_reason").ToLocalChecked(), Maps::getMap(Maps::eof_MAP));
    mobj->Set(Nan::New<String>("log_level").ToLocalChecked(), Maps::getMap(Maps::log_MAP));
    mobj->Set(Nan::New<String>("event_id").ToLocalChecked(), Maps::getMap(Maps::evid_MAP));
    return mobj;
  }
  
  mpv_handle* Core::get_core() { return main_ctx; }

  string Core::main_ctx_name() { return main_name; }

  mpv_handle* Core::player_ready(Local<Value> v8val, bool minit) {
    mpv_handle *ctx = nullptr;
    In ctx_name(v8val);

    bool prdy = (get_core() && clients.find(ctx_name.toStr()) != clients.end()); //ctx ready
    bool pinit = (core_status == 0); //ctx player initialized

    bool ready = (prdy && (minit && pinit)) || (prdy && !minit); 

    if (ready) {
      ctx = clients.find(ctx_name.toStr())->second;
    }
    return ctx;
  }

  map <string, mpv_handle*> Core::getClients() { return clients; }
  bool Core::getSuspended() { return core_suspended; }
  void Core::setSuspended(bool sus) { core_suspended = sus; }
}