#include "include/headers.hpp"

namespace libmpv {

  //////// Public methods /////////////////////////////
  // Constructors
  In::In() {}
  In::In(Local<Value> v8val) : seen(Nan::New<Array>()), rnode(makeNode(v8val)) {}

  // Destructors
  In::~In() {
    freeNode(rnode);
    Mpv::Mpv::log << "FREE ROOT NODE FORMAT : " << rnode->format << endl;
    delete rnode;
  }

  //methods
  mpv_node* In::toNode() { return rnode; }

  char* In::toStr() {
    char* empty = const_cast<char*>("");
    return (rnode->format == MPV_FORMAT_STRING || rnode->format == MPV_FORMAT_OSD_STRING) ?
      rnode->u.string : empty;
  }

  int In::toFlag() {
    return (rnode->format == MPV_FORMAT_FLAG) ?
      rnode->u.flag : 0;
  }

  int64_t In::toInt() {
    return (rnode->format == MPV_FORMAT_INT64) ?
      rnode->u.int64 : 0; 
  }

  double In::toDouble() {
    return (rnode->format == MPV_FORMAT_DOUBLE) ?
      rnode->u.double_ : 0;
  }

  void* In::toData() { return Out::getDataFromNode(rnode); }

  /////////////// Private methods ////////////////////////
  //add new v8 <Value> to seen Array of <Values>
  bool In::addToSeen(Local<Value> v8val) {
    int len = seen->Length();
    if(indexOfSeen(v8val) == -1) {
      Nan::Set(seen->ToObject(), len, v8val);
      return true;
    } else {
      return false;
    }
  }

  //check get indexOf v8val <Value> in seen
  int In::indexOfSeen(Local<Value> v8val) {
    int len = seen->ToObject().As<Array>()->Length();
    for (int i = 0; i < len; i++) {
      if(v8val == Nan::Get(seen->ToObject(), i).ToLocalChecked()) {
        return i;
      }
    }
    return -1;
  }

  //detect mpv_format
  mpv_format In::formatDetect(Local<Value> v8val) {
    mpv_format fmt = MPV_FORMAT_NONE;
    if(v8val->IsUndefined() || v8val->IsNull()) { //EMPTY node
      fmt = MPV_FORMAT_NONE;
    } else if(v8val->IsString()) { //STRING node
      fmt = MPV_FORMAT_STRING;
    } else if(v8val->IsBoolean()) { //FLAG node
      fmt = MPV_FORMAT_FLAG;
    } else if(v8val->IsInt32()) { //INT64 node
      fmt = MPV_FORMAT_INT64;
    } else if(v8val->IsNumber()) { //DOUBLE node
      fmt = MPV_FORMAT_DOUBLE;
    } else if(v8val->IsObject()) { // NODE_MAP or NODE_ARRAY array node
      Local<String> jscn = v8val->ToObject()->GetConstructorName(); //get constructor name
      //string cname = *String::Utf8Value(jscn);
      Nan::Utf8String cnmval(jscn);
      string cname = string(*cnmval);
      
      if(cname == "Buffer" || cname == "Uint8Array") { //Buffer -> byte-array node
        fmt = MPV_FORMAT_BYTE_ARRAY;
      } else { //mpv_node_list ARRAY or MAP
        if(v8val->IsArray() && cname == "Array") { 
          fmt = MPV_FORMAT_NODE_ARRAY;	
        } else { //mpv_node_list MAP
          fmt = MPV_FORMAT_NODE_MAP;
        }
      }
    }
    return fmt;
  }

  char* In::valueToCstr(Local<Value> v8val) {
    Nan::Utf8String v8str(v8val);
    int len = v8str.length() + 1;
    char* str_val = *v8str; //char* data from v8str
    char* str = new char[len];
    strcpy(str, str_val);
    Mpv::Mpv::log << "CREATE STRING data: " << str << endl;
    return str;
  }

  int In::valueToFlag(Local<Value> v8val) {
    bool b = Nan::To<bool>(v8val).FromJust();
    Mpv::Mpv::log << "CREATE FLAG data: " << b << endl;
    return static_cast<int>(b);
  }

  int64_t In::valueToInt64(Local<Value> v8val) {
    int64_t i = Nan::To<int64_t>(v8val).FromJust();
    Mpv::Mpv::log << "CREATE INT64 data: " << i << endl;
    return i;
  }

  double In::valueToDouble(Local<Value> v8val) {
    double d = Nan::To<double>(v8val).FromJust();
    Mpv::Mpv::log << "CREATE DOUBLE data: " << d << endl;
    return d;
  }

  mpv_node* In::makeNode(Local<Value> v8val) {
    mpv_node* nod = new mpv_node();
    nod->format = formatDetect(v8val);
    Mpv::Mpv::log << "CREATE NODE FORMAT: " << nod->format << endl;

    if(nod->format == MPV_FORMAT_STRING || nod->format == MPV_FORMAT_OSD_STRING) { //string node
      nod->u.string = valueToCstr(v8val);
    } else if(nod->format == MPV_FORMAT_FLAG) { //flag node
      nod->u.flag = valueToFlag(v8val);
    } else if(nod->format == MPV_FORMAT_INT64) { //int64 node
      nod->u.int64 = valueToInt64(v8val);
    } else if(nod->format == MPV_FORMAT_DOUBLE) { //double node
      nod->u.double_ = valueToDouble(v8val);
    } else if(nod->format == MPV_FORMAT_NODE_ARRAY || nod->format == MPV_FORMAT_NODE_MAP) {
      int sidx = indexOfSeen(v8val);
      if(sidx == -1) {
        addToSeen(v8val);
        nod->u.list = makeNodeList(v8val); //make mpv_node_list
      } else {
        Mpv::Mpv::log << "CYCLIC REFERENCE OBJECT INDEX: " << sidx << endl;
        nod->format = MPV_FORMAT_STRING;
        const char* s = "[Circular]";
        char* cstr = new char[strlen(s) + 1];
        strcpy(cstr, s);
        nod->u.string = cstr;
      }
    } else if (nod->format == MPV_FORMAT_BYTE_ARRAY) {
      nod->u.ba = valueToByteArray(v8val); //mape mpv_byte_array
    }
    return nod;
  }

  mpv_node_list* In::makeNodeList(Local<Value> v8val) {
    mpv_node_list* nlst = new mpv_node_list;
    Nan::MaybeLocal<Array> mokeys = Nan::GetOwnPropertyNames(v8val->ToObject());
    Local<Array> okeys = mokeys.IsEmpty() ? Nan::New<Array>() : mokeys.ToLocalChecked();
    int len = okeys->Length();
    nlst->num = len; //fill length

    //create mpv_node[] without keys
    nlst->values = new mpv_node[len];
    for(int i = 0; i < len; i++) {
      Local<Value> nodeob = v8val->ToObject()->Get(okeys->Get(i));
      mpv_node* nod = makeNode(nodeob);
      //add node to list if not type NONE
      nlst->values[i] = *nod;
    }

    //nlst->keys NULL for NODE_ARRAY or add nlst->keys strings for NODE_MAP
    if(v8val->IsArray()) { //only convert each element
      nlst->keys = nullptr;
    } else if (v8val->IsObject()) {
      nlst->keys = new char*[len]; //make c++ array of keys
      for(int i = 0; i < len; i++) {
        nlst->keys[i] = valueToCstr(okeys->Get(i));
      }
    }

    return nlst;
  }

  mpv_byte_array* In::valueToByteArray(Local<Value> v8val) {
    mpv_byte_array *bar = new mpv_byte_array;
    bar->data = static_cast<void*>(node::Buffer::Data(v8val->ToObject()));
    bar->size = node::Buffer::Length(v8val->ToObject());
    Mpv::Mpv::log << "CREATE BYTE_ARRAY data: " << bar->data << endl;
    return bar;
  }

  void In::freeNode(mpv_node* node) {
    if(node->format == MPV_FORMAT_STRING || node->format == MPV_FORMAT_OSD_STRING) {
      Mpv::Mpv::log << "FREE ALLOCATED NODE VALUE STRING: " << node->u.string << endl;
      delete[] node->u.string;
    } else if (node->format == MPV_FORMAT_NODE_MAP || node->format == MPV_FORMAT_NODE_ARRAY) {
      freeNodeList(node->u.list, node->format);
    }
  }

  void In::freeNodeList(mpv_node_list* nlst, mpv_format format) {
    int len = nlst->num;
    Mpv::Mpv::log << "FREE ALLOCATED mpv_node_list length: " << nlst->num << endl;
    for (int i = 0; i < len; i++) {
      Mpv::Mpv::log << "FREE NODE at index : " << i << " FORMAT : " << *&nlst->values[i].format << endl;
      freeNode(&nlst->values[i]);
      if (format == MPV_FORMAT_NODE_MAP) {
        Mpv::Mpv::log << "FREE ALLOCATED NODE_MAP KEY STRING: " << nlst->keys[i] << endl;
        delete[] nlst->keys[i]; //*char sting key
      }
    }
    //node_list arrays
    if (format == MPV_FORMAT_NODE_MAP) {
      delete[] nlst->keys; //**char array (of keys)
    }
    delete[] nlst->values; //*mpv_node array (of values -> mpv_node..s)
    delete nlst; //delete node_list structure
  }

}