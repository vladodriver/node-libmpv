#include "include/headers.hpp"

namespace libmpv {

  //////Class Evout /////////////
  //public
  Evout::Evout(mpv_event* ev) : event(ev), eval(createEv(event)) {}

  Evout::~Evout() {}
  Local<Value> Evout::value() { return eval; }

  //private
  Local<Value> Evout::createEv(mpv_event* ev) {
    Local<Value> jsev = Nan::New<Object>();

    //get event name
    jsev->ToObject()->Set(
      Nan::New<String>("eventId").ToLocalChecked(),
      Nan::New<Int32>(static_cast<int32_t>(ev->event_id))
    );
    jsev->ToObject()->Set(
      Nan::New<String>("eventName").ToLocalChecked(),
      Nan::New<String>(mpv_event_name(ev->event_id)).ToLocalChecked()
    );

    //get error status
    Local<Value> errval; //set error js value
    if(event->error == 0) {
      errval = Nan::New<Boolean>(false);
    } else {
      errval = Nan::New<String>(mpv_error_string(event->error)).ToLocalChecked();
    }
    jsev->ToObject()->Set(Nan::New<String>("error").ToLocalChecked(), errval);
    jsev->ToObject()->Set(
      Nan::New<String>("errorCode").ToLocalChecked(),
      Nan::New<Int32>(ev->error)
    );

    //get reply_userdata
    jsev->ToObject()->Set(
      Nan::New<String>("replyUserdata").ToLocalChecked(),
      Nan::New<Uint32>(static_cast<uint32_t>(event->reply_userdata))
    );

    //get data from void* p.
    if(event->data) { 
      jsev->ToObject()->Set(
        Nan::New<String>("data").ToLocalChecked(), //fill data property in parsed event
        addEvData(event->data, event->event_id)->ToObject()->Get(
          Nan::New<String>("data").ToLocalChecked())
      );
    }
    return jsev;
  }

  Local<Value> Evout::addEvData(void* data, mpv_event_id evid) {
    Local<Value> evdata = Nan::New<Object>();
    Evdata evd(data, evid);

    evdata->ToObject()->Set(
      Nan::New<String>("data").ToLocalChecked(),
      evd.value()
    );

    return evdata;
  }

  //// Class Evdata -> parser void* mpv_event->data ////
  //public
  Evdata::Evdata(void* evd, mpv_event_id evid) : evout(makeEvdata(evd, evid)) {}
  Evdata::~Evdata() {}
  Local<Value> Evdata::value() { return evout; }

  //private
  Local<Value> Evdata::makeEvdata(void* evd, mpv_event_id evid) {
    Mpv::log << ">> Parse mpv_event DATA started: <<" << endl;
    Local<Value> evdata;
    if (evid == MPV_EVENT_GET_PROPERTY_REPLY || evid == MPV_EVENT_PROPERTY_CHANGE) {
      Mpv::Mpv::log << ">> -> PROPERTY event <<" << endl;
      evdata = parseEvData(static_cast<mpv_event_property*>(evd));
    } else if (evid == MPV_EVENT_LOG_MESSAGE) {
      Mpv::log << ">> -> LOG_MESSAGE event <<" << endl;
      evdata = parseEvData(static_cast<mpv_event_log_message*>(evd));
    } else if (evid == MPV_EVENT_CLIENT_MESSAGE) {
      Mpv::Mpv::log << ">> -> CLIENT_MESSAGE event <<" << endl;
      evdata = parseEvData(static_cast<mpv_event_client_message*>(evd));
    } else if (evid == MPV_EVENT_END_FILE) {
      Mpv::Mpv::log << ">> -> EVENT_END_FILE event <<" << endl;
      evdata = parseEvData(static_cast<mpv_event_end_file*>(evd));
    } else {
      Mpv::Mpv::log << ">> -> EVENT wthout any DATA <<" << endl;
      evdata = Nan::Undefined();
    }
    return evdata;
  }

  Local<Value> Evdata::parseEvData(mpv_event_property* evp) {
    Local<Value> jsevp = Nan::New<Object>();
    Fmt<mpv_format> fmt(Maps::for_MAP, evp->format);

    Out evo; //declare 
    if (fmt.format() != MPV_FORMAT_NODE) {
      evo = Out(evp->data, fmt.format());
    } else {
      evo = Out(static_cast<mpv_node*>(evp->data));
    }

    jsevp->ToObject()->Set(
      Nan::New<String>("name").ToLocalChecked(),
      Nan::New<String>(evp->name).ToLocalChecked()
    );
    jsevp->ToObject()->Set(
      Nan::New<String>("format").ToLocalChecked(),
      Nan::New<String>(fmt.cstr()).ToLocalChecked()
    );
    jsevp->ToObject()->Set(
      Nan::New<String>("result").ToLocalChecked(),
      evo.value()
    );
    return jsevp;
  }

  Local<Value> Evdata::parseEvData(mpv_event_log_message* evlm) {
    Local<Value> jsevl = Nan::New<Object>();
    Fmt<mpv_log_level> logl(Maps::log_MAP, evlm->log_level);
    jsevl->ToObject()->Set(
      Nan::New<String>("prefix").ToLocalChecked(),
      Nan::New<String>(evlm->prefix).ToLocalChecked()
    );
    jsevl->ToObject()->Set(
      Nan::New<String>("level").ToLocalChecked(),
      Nan::New<String>(evlm->level).ToLocalChecked()
    );
    jsevl->ToObject()->Set(
      Nan::New<String>("text").ToLocalChecked(),
      Nan::New<String>(evlm->prefix).ToLocalChecked()
    );
    jsevl->ToObject()->Set(
      Nan::New<String>("mpvLogLevel").ToLocalChecked(),
      Nan::New<String>(logl.cstr()).ToLocalChecked()
    );
    return jsevl;
  }

  Local<Value> Evdata::parseEvData(mpv_event_end_file* eveof) {
    Local<Value> jsevef = Nan::New<Object>();
    Fmt<mpv_end_file_reason> re(Maps::eof_MAP, static_cast<mpv_end_file_reason>(eveof->reason));

    jsevef->ToObject()->Set(
      Nan::New<String>("reason").ToLocalChecked(),
      Nan::New<String>(re.cstr()).ToLocalChecked()
    );

    Local<Value> eoferval;
    if(eveof->error == 0) {
      eoferval = Nan::New<Boolean>(false);
    } else {
      eoferval = Nan::New<String>(mpv_error_string(eveof->error)).ToLocalChecked();
    }
    jsevef->ToObject()->Set(Nan::New<String>("error").ToLocalChecked(), eoferval);
    jsevef->ToObject()->Set(
      Nan::New<String>("errorCode").ToLocalChecked(),
      Nan::New<Int32>(eveof->error)
    );
    return jsevef;
  }

  Local<Value> Evdata::parseEvData(mpv_event_script_input_dispatch* evsid) {
    Local<Value> jsevsid = Nan::New<Object>();
    jsevsid->ToObject()->Set(
      Nan::New<String>("arg0").ToLocalChecked(),
      Nan::New<Int32>(evsid->arg0)
    );
    jsevsid->ToObject()->Set(
      Nan::New<String>("type").ToLocalChecked(),
      Nan::New<String>(evsid->type).ToLocalChecked()
    );
    return jsevsid;
  }

  Local<Value> Evdata::parseEvData(mpv_event_client_message* evcm) {
    Local<Value> jsevcm = Nan::New<Object>();
    Arval jsar(evcm->args, evcm->num_args);
    jsevcm->ToObject()->Set(
      Nan::New<String>("numArgs").ToLocalChecked(),
      Nan::New<Int32>(evcm->num_args)
    );
    jsevcm->ToObject()->Set(
      Nan::New<String>("args").ToLocalChecked(),
      jsar.value()
    );
    return jsevcm;
  }

} //namespace libmpv