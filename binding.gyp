{
  "targets": [
    {
      "target_name": "mpv",
      "libraries": ["-lmpv"],
			"cflags": ["-std=c++11 -Wno-cast-function-type"],
      "sources": ["cmds.cpp", "core.cpp", "evout.cpp", "in.cpp", "mpv.cpp", "maps.cpp", "out.cpp"],
			"include_dirs": [
        "<!(node -e \"require('nan')\")"
      ]
    }
  ]
}
