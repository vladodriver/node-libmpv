//load module to mod
'use strict';
var fs = require('fs');
var Api = require('./build/Release/mpv').Mpv;

//get version of module from package.jsons
function getVersion() {
  var ver;
  try {
    ver = JSON.parse(fs.readFileSync('./package.json').toString()).version;
  } catch (err) {
    ver = 'Parsing version failed : ' + err.message;
  }
  return ver;
}

///// hidden Classes/////////////

/** Main one player (window) class 
 * Create one player lov level api and ctxs property object
 * which store api core and clients in Ctx instance
*/
function Player(name, dbg) {
  //get version
  Object.defineProperty(this, 'version', { enumerable: true, value: getVersion() });
  Object.defineProperty(this, 'name', { enumerable: true, value: name });
  Object.defineProperty(this, '__api', { value: Object.freeze(new Api(dbg)) });
  
  //all ctxs
  Object.defineProperty(this, 'ctx', { enumerable: true, value: Object.create(null)});
  //player status from __api
  Object.defineProperty(this, 'status', { enumerable: true,
  	get: function status() {
  		return this.__api.status;
  	}
  });
  
  //create ctx
  this.__api.create(); //create core ctx
  //TODO REPLACE with client sync
  console.log('THIS API IN PLAYER STATUS',this.__api.status);
  Object.defineProperty(this.ctx, this.__api.status.coreClientName, {
    enumerable: true, configurable: true,
    value: new CtxApi(this, this.__api.status.coreClientName, this.__api)
  });
  
  return this;//
}

//manipulate ctx object when call 'detachDestroy', 'terminateDestroy' and 'createClient'
Player.prototype._ctxSync = function _ctxSync() {
	//TODO zjisti status a vytvoři nove instance CtxApi, nebo smaže neexistujici
	var stat = this.__api.status;
	console.log('TODO CTX SYNC');
	
};

//Class create CtxApi api instance with new prototype of Api methods
function CtxApi(player, ctxname) {
	Object.defineProperty(this, '__player', { value: player });
  Object.defineProperty(this, 'ctxName', { enumerable: true, value: ctxname });
  
  Object.defineProperty(this, 'ctxPointer', {
    enumerable: true, value: this.__player.__api.status.clients[this.ctxName] 
  });
}

// /// Prototype ctx make functions /// //



//are special fns -> need Player class logic
var ctx_core_fn = ['detachDestroy', 'terminateDestroy'];
for (var i = 0; i < ctx_core_fn.length; i++) {
	var fn_name = ctx_core_fn[i];
	Object.defineProperty(CtxApi.prototype, fn_name, {
		value: function() {
			var args = [this.ctxName].concat(Array.prototype.slice.call(arguments, 1));
			var res = this.__player.__api[fn_name].apply(this.__player.__api, args);
			
			this.__player._ctxSync();	
		
		
			return res;
		}
	});
}



///// Static simple api ////////////////////////
var mpvStatic = {
  players: {
    enumerable: true,
    value: Object.create(null)
  },
  createPlayer: {
    value: function createPlayer(name, dbg) {
      if (!(name in this.players)) {
        Object.defineProperty(this.players, name, {
          configurable: true, enumerable: true, value: new Player(name, dbg)
        });
      }
      return this.players;
    }
  },
  destroyPlayer: {
  	value: function createPlayer(name) {
      if (name in this.players) {
        delete this.players[name];
      }
      return this.players;
    }
  }
};

//TODO new concept players are private and exp have getter fn for get players...

//Export low-level Api and high-level Mpv object
var exp = Object.create(null, {
  Api: { enumerable: true, value: Api},
  Mpv: { enumerable:true, value: Object.create(null, mpvStatic) }
});

//console.log(exp);

module.exports = exp;