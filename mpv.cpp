#include "include/headers.hpp"

namespace libmpv {

  Mpv::Mpv(Core c) : core(c) {} //constructor
  Mpv::~Mpv() {} //destructor

  //static JS Construction Function for New()
  Nan::Persistent<Function> Mpv::constructor;

  //static own debug logging system method
  Log Mpv::log;

  //Getter for Mpv.status property
  void Mpv::mpv_status_getter(Local<String> prop, const Nan::PropertyCallbackInfo<Value>& args) {
    Mpv *mpv = Nan::ObjectWrap::Unwrap<Mpv>(args.Holder());
    args.GetReturnValue().Set(mpv->core.status());
  }
  
  //Getter for Mpv.enums property
  void Mpv::mpv_enums_getter(Local<String> prop, const Nan::PropertyCallbackInfo<Value>& args) {
    Mpv *mpv = Nan::ObjectWrap::Unwrap<Mpv>(args.Holder());
    args.GetReturnValue().Set(mpv->core.getMaps());
  }
  
  //Module debugging on/off
  void Mpv::debug(const Nan::FunctionCallbackInfo<Value>& args) {
    bool debug = Nan::To<bool>(args[0]).FromJust();
    Local<Boolean> state;

    if(debug) {
      Mpv::log.on();
      state = Nan::New<Boolean>(true);
    } else {
      Mpv::log.off();
      state = Nan::New<Boolean>(false);
    }
    args.GetReturnValue().Set(state);
  }

  ////////////// API START///////////////////////////////////////////////////////////////
  //client api version number
  void Mpv::client_api_version(const Nan::FunctionCallbackInfo<Value>& args) {
    Local<Value> num = Nan::New<Uint32>(static_cast<uint32_t>(mpv_client_api_version()));
    args.GetReturnValue().Set(num);
  }

  //convert error number to string form
  void Mpv::error_string(const Nan::FunctionCallbackInfo<Value>& args) {
    int32_t num = Nan::To<int32_t>(args[0]).FromJust();
    const char* erstr = mpv_error_string(static_cast<int>(num));
    args.GetReturnValue().Set(Nan::New<String>(erstr).ToLocalChecked());
  }

  //create core ctx and return status
  void Mpv::create(const Nan::FunctionCallbackInfo<Value>& args) {
    Mpv *mpv = Nan::ObjectWrap::Unwrap<Mpv>(args.Holder());
    mpv->core.create(args);
    args.GetReturnValue().Set(mpv->core.status());
  }

  //initialize player for playing after (ex. after setting options)
  void Mpv::initialize(const Nan::FunctionCallbackInfo<Value>& args) {
    Mpv *mpv = Nan::ObjectWrap::Unwrap<Mpv>(args.Holder());
    args.GetReturnValue().Set(mpv->core.initialize(args));
  }

  //detach player client and destroy if no other  clients
  void Mpv::detach_destroy(const Nan::FunctionCallbackInfo<Value>& args) {
    Mpv *mpv = Nan::ObjectWrap::Unwrap<Mpv>(args.Holder());
    args.GetReturnValue().Set(mpv->core.destroyHandle(args, "detach"));
  }

  //detach player client and destroy all clients and terminate player
  void Mpv::terminate_destroy(const Nan::FunctionCallbackInfo<Value>& args) {
    Mpv *mpv = Nan::ObjectWrap::Unwrap<Mpv>(args.Holder());
    args.GetReturnValue().Set(mpv->core.destroyHandle(args, "terminate"));
  }

  //create new client shared core ctx (same player)
  void Mpv::create_client(const Nan::FunctionCallbackInfo<Value>& args) {
    Mpv *mpv = Nan::ObjectWrap::Unwrap<Mpv>(args.Holder());
    args.GetReturnValue().Set(mpv->core.createClient(args));
  }

  //load config file -> need before initialize
  void Mpv::load_config_file(const Nan::FunctionCallbackInfo<Value>& args) {
    Mpv *mpv = Nan::ObjectWrap::Unwrap<Mpv>(args.Holder());
    Result res;

    mpv_handle* ctx = mpv->core.player_ready(args[0], false);
    Nan::Utf8String v8str(args[1]);
    const char* apath = *v8str;

    if(ctx) {
      int err = mpv_load_config_file(ctx, apath);
      res.addError(err);
    } else {
      res.addError(-3);
    }
    args.GetReturnValue().Set(res.getRes());
  }

  //suspend playback thread
  void Mpv::suspend(const Nan::FunctionCallbackInfo<Value>& args) {
    Mpv *mpv = Nan::ObjectWrap::Unwrap<Mpv>(args.Holder());
    mpv_handle* ctx = mpv->core.player_ready(args[0], true);

    if(ctx) {
      mpv_suspend(ctx);
      mpv->core.setSuspended(true);
    }
    args.GetReturnValue().Set(mpv->core.status());
  }

  //resume playback thread
  void Mpv::resume(const Nan::FunctionCallbackInfo<Value>& args) {
    Mpv *mpv = Nan::ObjectWrap::Unwrap<Mpv>(args.Holder());
    mpv_handle* ctx = mpv->core.player_ready(args[0], false);

    if(ctx) {
      mpv_resume(ctx);
      mpv->core.setSuspended(false);
    }
    args.GetReturnValue().Set(mpv->core.status());
  }

  //get time us (microsecconds)
  void Mpv::get_time_us(const Nan::FunctionCallbackInfo<Value>& args) {
    Mpv *mpv = Nan::ObjectWrap::Unwrap<Mpv>(args.Holder());
    Result res;
    int err = -3;
    mpv_handle* ctx = mpv->core.player_ready(args[0], false);

    if (ctx) {
      int64_t time = mpv_get_time_us(ctx);
      log << "Time us : " << time << endl;
      //args.GetReturnValue().Set(Nan::New<Number>(static_cast<double>(time)));
      err = 0;
      res.addData(Nan::New<Number>(static_cast<double>(time)));
    }
    res.addError(err);
    args.GetReturnValue().Set(res.getRes());
  }

  //set any option in any mpv_format and void* data
  void Mpv::set_option(const Nan::FunctionCallbackInfo<Value>& args) {
    Mpv *mpv = Nan::ObjectWrap::Unwrap<Mpv>(args.Holder());
    args.GetReturnValue().Set(mpv->set_("option", args));
  }

  //set any option as string form
  void Mpv::set_option_string(const Nan::FunctionCallbackInfo<Value>& args) {
    Mpv *mpv = Nan::ObjectWrap::Unwrap<Mpv>(args.Holder());
    Result res;
    mpv_handle *ctx = mpv->core.player_ready(args[0], false);
    if (ctx) {
      In key(args[1]);
      In val(args[2]);
      int err = mpv_set_option_string(ctx, key.toStr(), val.toStr());
      res.addError(err);
    } else {
      res.addError(-3);
    }
    args.GetReturnValue().Set(res.getRes());
  }

  //call command as array of strings NULL terminated
  void Mpv::command(const Nan::FunctionCallbackInfo<Value>& args) {
    Mpv *mpv = Nan::ObjectWrap::Unwrap<Mpv>(args.Holder());
    args.GetReturnValue().Set(mpv->cmd_(args, false));
  }

  //call command in mpv_node format
  void Mpv::command_node(const Nan::FunctionCallbackInfo<Value>& args) {
    Mpv *mpv = Nan::ObjectWrap::Unwrap<Mpv>(args.Holder());
    args.GetReturnValue().Set(mpv->cmd_node_(args, false));
  }

  //call command as one string
  void Mpv::command_string(const Nan::FunctionCallbackInfo<Value>& args) {
    Mpv *mpv = Nan::ObjectWrap::Unwrap<Mpv>(args.Holder());
    In cmd_str(args[1]);
    Result res;
    mpv_handle* ctx = mpv->core.player_ready(args[0]);
    if(ctx) {
      int err = mpv_command_string(ctx, cmd_str.toStr());
      res.addError(err);
    } else {
      res.addError(-3);
    }
    args.GetReturnValue().Set(res.getRes());
  }

  //async version of mpv_command
  void Mpv::command_async(const Nan::FunctionCallbackInfo<Value>& args) {
    Mpv *mpv = Nan::ObjectWrap::Unwrap<Mpv>(args.Holder());
    args.GetReturnValue().Set(mpv->cmd_(args, true));
  }

  //async version of mpv_command_node
  void Mpv::command_node_async(const Nan::FunctionCallbackInfo<Value>& args) {
    Mpv *mpv = Nan::ObjectWrap::Unwrap<Mpv>(args.Holder());
    args.GetReturnValue().Set(mpv->cmd_node_(args, true));
  }

  //set any property in any mpv_format and void* data
  void Mpv::set_property(const Nan::FunctionCallbackInfo<Value>& args) {
    Mpv *mpv = Nan::ObjectWrap::Unwrap<Mpv>(args.Holder());
    args.GetReturnValue().Set(mpv->set_("property", args));
  }

  //set property in string foem
  void Mpv::set_property_string(const Nan::FunctionCallbackInfo<Value>& args) {
    Mpv *mpv = Nan::ObjectWrap::Unwrap<Mpv>(args.Holder());
    args.GetReturnValue().Set(mpv->set_("property-string", args));
  }

  //async version mpv_set_property
  void Mpv::set_property_async(const Nan::FunctionCallbackInfo<Value>& args) {
    Mpv *mpv = Nan::ObjectWrap::Unwrap<Mpv>(args.Holder());
    args.GetReturnValue().Set(mpv->set_("property-async", args));
  }

  //get any property in automatic native format, or select own
  void Mpv::get_property(const Nan::FunctionCallbackInfo<Value>& args) {
    Mpv *mpv = Nan::ObjectWrap::Unwrap<Mpv>(args.Holder());
    args.GetReturnValue().Set(mpv->get_property_(args, false));
  }

  //get any property as string format
  void Mpv::get_property_string(const Nan::FunctionCallbackInfo<Value>& args) {
    Mpv *mpv = Nan::ObjectWrap::Unwrap<Mpv>(args.Holder());
    args.GetReturnValue().Set(mpv->get_property_str_("string", args));
  }

  //get any property as osd-string format
  void Mpv::get_property_osd_string(const Nan::FunctionCallbackInfo<Value>& args) {
    Mpv *mpv = Nan::ObjectWrap::Unwrap<Mpv>(args.Holder());
    args.GetReturnValue().Set(mpv->get_property_str_("osd-string", args));
  }

  //async version mpv_get_property
  void Mpv::get_property_async(const Nan::FunctionCallbackInfo<Value>& args) {
    Mpv *mpv = Nan::ObjectWrap::Unwrap<Mpv>(args.Holder());
    args.GetReturnValue().Set(mpv->get_property_(args, true));
  }

  //observe property use async mpv_event
  void Mpv::observe_property(const Nan::FunctionCallbackInfo<Value>& args) {
    Mpv *mpv = Nan::ObjectWrap::Unwrap<Mpv>(args.Holder());
    Result res;

    mpv_handle* ctx = mpv->core.player_ready(args[0]);
    In rudata(args[1]); 
    In prop(args[2]);
    In fmtstr(args[2]);
    Fmt <mpv_format> fmtval(Maps::for_MAP, fmtstr.toStr());
    mpv_format fmt = (fmtval.format() != MPV_FORMAT_NONE) ? fmtval.format() : MPV_FORMAT_NODE;

    if(ctx) {
      res.addError(mpv_observe_property(ctx, abs(rudata.toInt()), prop.toStr(), fmt));
    } else {
      res.addError(-3);
    }
    args.GetReturnValue().Set(res.getRes());
  }

  //unobserve property
  void Mpv::unobserve_property(const Nan::FunctionCallbackInfo<Value>& args) {
    Mpv *mpv = Nan::ObjectWrap::Unwrap<Mpv>(args.Holder());
    Result res;

    mpv_handle* ctx = mpv->core.player_ready(args[0]);
    In rudata(args[1]); 

    if(ctx) {
      res.addError(mpv_unobserve_property(ctx, abs(rudata.toInt())));
    } else {
      res.addError(-3);
    }
    args.GetReturnValue().Set(res.getRes());
  }

  //convert mpv_event_id to mpv_event name string
  void Mpv::event_name(const Nan::FunctionCallbackInfo<Value>& args) {
    int32_t idn = Nan::To<int32_t>(args[0]).FromJust();

    //check range same as mpv_event_id enum and correction >=0 || <= 24
    idn = (idn >= 0) ? idn : 0; //must be >= 0 -> crash
    idn = (idn <= 24) ? idn : 0; // must be <= 24 -> crash

    mpv_event_id mevid = static_cast<mpv_event_id>(idn);
    const char* str = mpv_event_name(mevid);
    args.GetReturnValue().Set(Nan::New<String>(str).ToLocalChecked());
  }

  //request (enable/disable) any event type (string foem)
  void Mpv::request_event(const Nan::FunctionCallbackInfo<Value>& args) {
    Mpv *mpv = Nan::ObjectWrap::Unwrap<Mpv>(args.Holder());
    Result res;
    mpv_handle* ctx = mpv->core.player_ready(args[0]);

    if(ctx) {
      In eviOut(args[1]);
      In enable(args[2]);

      Fmt<mpv_event_id> evid(Maps::evid_MAP, eviOut.toStr());
      res.addError(mpv_request_event(ctx, evid.format(), enable.toFlag()));
    } else {
      res.addError(-3);
    }
    args.GetReturnValue().Set(res.getRes());
  }

  //request any log messageslevel use mpv_event 
  void Mpv::request_log_messages(const Nan::FunctionCallbackInfo<Value>& args) {
    Mpv *mpv = Nan::ObjectWrap::Unwrap<Mpv>(args.Holder());
    Result res;
    mpv_handle* ctx = mpv->core.player_ready(args[0]);

    if(ctx) {	
      In loglval(args[1]);
      res.addError(mpv_request_log_messages(ctx, loglval.toStr()));
    } else {
      res.addError(-3);
    }
    args.GetReturnValue().Set(res.getRes());
  }

  // Asynchronous wait for next mpv_event and return it
  void Mpv::wait_event(const Nan::FunctionCallbackInfo<Value>& args) {
    Mpv *mpv = Nan::ObjectWrap::Unwrap<Mpv>(args.Holder());
    Result res;
    mpv_handle* ctx = mpv->core.player_ready(args[0]);

    if (ctx) {
      res.addError(0);
      double to = Nan::To<double>(args[1]).FromJust();
      
      //run AsyncQueueWorker only if callback set
      if(args[2]->IsFunction()) { //check if Callback defined
      	Nan::Callback *callback = new Nan::Callback(args[2].As<Function>());
      	AsyncQueueWorker(new Async(callback, ctx, to));
      }
    } else {
      res.addError(-3);
    }
    args.GetReturnValue().Set(res.getRes());
  }

  //interrupt waiting of next event
  void Mpv::wakeup(const Nan::FunctionCallbackInfo<Value>& args) {
    Mpv *mpv = Nan::ObjectWrap::Unwrap<Mpv>(args.Holder());
    mpv_handle* ctx = mpv->core.player_ready(args[0]);
    if (ctx) {
      mpv_wakeup(ctx);
    }
    args.GetReturnValue().Set(mpv->core.status());
  }

  //unix_file_descriptor number (or -1 on Windows and error)
  void Mpv::get_wakeup_pipe(const Nan::FunctionCallbackInfo<Value>& args) {
    Mpv *mpv = Nan::ObjectWrap::Unwrap<Mpv>(args.Holder());
    mpv_handle* ctx = mpv->core.player_ready(args[0]);
    int descriptor = -1;
    if (ctx) {
      descriptor = mpv_get_wakeup_pipe(ctx);
    }
    args.GetReturnValue().Set(Nan::New<Int32>(descriptor));
  }

  //block until all asynchronous requests are done
  void Mpv::wait_async_requests(const Nan::FunctionCallbackInfo<Value>& args) {
    Mpv *mpv = Nan::ObjectWrap::Unwrap<Mpv>(args.Holder());
    mpv_handle* ctx = mpv->core.player_ready(args[0]);
    if (ctx) {
      mpv_wait_async_requests(ctx);
    }
    args.GetReturnValue().Set(mpv->core.status());
  }

  //... secret ! for any test cases ... !
  void Mpv::_test(const Nan::FunctionCallbackInfo<Value>& args) {
    //Mpv *mpv = Nan::ObjectWrap::Unwrap<Mpv>(args.Holder());
  	Local<Value> v = args[0];
    //cout << "INIT client ctx err: " << mpv_initialize(client) << endl;
    double to = Nan::To<double>(v).FromJust();
    
    //cout << "CTX AGAIN init with err: " << mpv_initialize(ctx) << endl;
    args.GetReturnValue().Set(to);
  }

  //Module constructor mpv = require('mpv').Mpv() or mpv = new Mpv();
  void Mpv::New(const Nan::FunctionCallbackInfo<Value>& args) {

    // Invoked as constructor new Mpv()
    if (args.IsConstructCall()) {
      //turn on/off debugging
      bool dbg = Nan::To<bool>(args[0]).FromJust(); //debug on/off
      if(!dbg) {
        Mpv::log.off();
      } else {
        Mpv::log.on();
      }

      Core c;
      Mpv *mpv = new Mpv(c);
      mpv->Wrap(args.This());
      
      //add "status" mpv_enums_getter
      Nan::SetAccessor(args.This(), Nan::New<String>("status").ToLocalChecked(),
        mpv->mpv_status_getter, 0, Local<Value>(), PROHIBITS_OVERWRITING, ReadOnly);
      //add "maps" mpv_enums_getter
      Nan::SetAccessor(args.This(), Nan::New<String>("enums").ToLocalChecked(),
        mpv->mpv_enums_getter, 0, Local<Value>(), PROHIBITS_OVERWRITING, DontEnum);
      args.GetReturnValue().Set(args.This());
    } else {
      // Invoked as plain js function Mpv() 
      // -> turn into construct call new Mpv() create instance.
      Local<Value> argv[1] = { args[0] }; //for debug on/off
      Local<Function> cons = Nan::New<Function>(constructor);
      args.GetReturnValue().Set(Nan::NewInstance(cons, 1, argv).ToLocalChecked());
    }
  }

  //register module functions
  void Mpv::Init(Handle<Object> exports) {
    Nan::HandleScope scope;
    // Prepare constructor template
    Local<FunctionTemplate> tpl = Nan::New<FunctionTemplate>(New);
    tpl->SetClassName(Nan::New("Mpv").ToLocalChecked());
    tpl->ReadOnlyPrototype();
    tpl->InstanceTemplate()->SetInternalFieldCount(1);
    
    // Prototype methods
    Nan::SetPrototypeMethod(tpl, "debug", debug); //own
    Nan::SetPrototypeMethod(tpl, "_test", _test); //only for testing
    // API //
    Nan::SetPrototypeMethod(tpl, "clientApiVersion", client_api_version);
    Nan::SetPrototypeMethod(tpl, "errorString", error_string);
    Nan::SetPrototypeMethod(tpl, "create", create);
    Nan::SetPrototypeMethod(tpl, "initialize", initialize);
    Nan::SetPrototypeMethod(tpl, "detachDestroy", detach_destroy);
    Nan::SetPrototypeMethod(tpl, "terminateDestroy", terminate_destroy);
    Nan::SetPrototypeMethod(tpl, "createClient", create_client);
    Nan::SetPrototypeMethod(tpl, "loadConfigFile", load_config_file);
    Nan::SetPrototypeMethod(tpl, "suspend", suspend);
    Nan::SetPrototypeMethod(tpl, "resume", resume);
    Nan::SetPrototypeMethod(tpl, "getTimeUs", get_time_us);
    Nan::SetPrototypeMethod(tpl, "setOption", set_option);
    Nan::SetPrototypeMethod(tpl, "setOptionString", set_option_string);
    Nan::SetPrototypeMethod(tpl, "command", command);
    Nan::SetPrototypeMethod(tpl, "commandNode", command_node);
    Nan::SetPrototypeMethod(tpl, "commandString", command_string);
    Nan::SetPrototypeMethod(tpl, "commandAsync", command_async);
    Nan::SetPrototypeMethod(tpl, "commandNodeAsync", command_node_async);
    Nan::SetPrototypeMethod(tpl, "setProperty", set_property);
    Nan::SetPrototypeMethod(tpl, "setPropertyString", set_property_string);
    Nan::SetPrototypeMethod(tpl, "setPropertyAsync", set_property_async);
    Nan::SetPrototypeMethod(tpl, "getProperty", get_property);
    Nan::SetPrototypeMethod(tpl, "getPropertyString", get_property_string);
    Nan::SetPrototypeMethod(tpl, "getPropertyOsdString", get_property_osd_string);
    Nan::SetPrototypeMethod(tpl, "getPropertyAsync", get_property_async);
    Nan::SetPrototypeMethod(tpl, "observeProperty", observe_property);
    Nan::SetPrototypeMethod(tpl, "unobserveProperty", unobserve_property);
    Nan::SetPrototypeMethod(tpl, "eventName", event_name);
    Nan::SetPrototypeMethod(tpl, "requestEvent", request_event);
    Nan::SetPrototypeMethod(tpl, "requestLogMessages", request_log_messages);
    Nan::SetPrototypeMethod(tpl, "waitEvent", wait_event);
    Nan::SetPrototypeMethod(tpl, "wakeup", wakeup);
    Nan::SetPrototypeMethod(tpl, "getWakeupPipe", get_wakeup_pipe);
    Nan::SetPrototypeMethod(tpl, "waitAsyncRequests", wait_async_requests);
    // end of API //
    
    constructor.Reset(tpl->GetFunction());
    exports->Set(Nan::New<String>("Mpv").ToLocalChecked(), tpl->GetFunction());
  }

  void InitAll(Handle<Object> exports) {
    Mpv::Init(exports);
  }

  NODE_MODULE(mpv, InitAll)

} //namespace libmpv
