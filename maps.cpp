#include "include/headers.hpp"

namespace libmpv {

  map<string, mpv_format> Maps::for_MAP = {
    {"none", MPV_FORMAT_NONE},
    {"str", MPV_FORMAT_STRING},
    {"osd", MPV_FORMAT_OSD_STRING},
    {"bool", MPV_FORMAT_FLAG},
    {"int", MPV_FORMAT_INT64},
    {"float", MPV_FORMAT_DOUBLE},
    {"node", MPV_FORMAT_NODE},
    {"node-array", MPV_FORMAT_NODE_ARRAY},
    {"node-map", MPV_FORMAT_NODE_MAP},
    {"byte-array", MPV_FORMAT_BYTE_ARRAY}
  };

  map<string, mpv_end_file_reason> Maps::eof_MAP = {
    {"none", MPV_END_FILE_REASON_EOF},
    {"stop", MPV_END_FILE_REASON_STOP},
    {"quit", MPV_END_FILE_REASON_QUIT},
    {"error", MPV_END_FILE_REASON_ERROR},
    {"redirect", MPV_END_FILE_REASON_REDIRECT}
  };

  map<string, mpv_log_level> Maps::log_MAP = {
    {"no", MPV_LOG_LEVEL_NONE},
    {"fatal", MPV_LOG_LEVEL_FATAL},
    {"error", MPV_LOG_LEVEL_ERROR},
    {"warn", MPV_LOG_LEVEL_WARN},
    {"info", MPV_LOG_LEVEL_INFO},
    {"v", MPV_LOG_LEVEL_V},
    {"debug", MPV_LOG_LEVEL_DEBUG},
    {"trace", MPV_LOG_LEVEL_TRACE}
  };

  map<string, mpv_event_id> Maps::evid_MAP = {
    {"none", MPV_EVENT_NONE},
    {"shutdown", MPV_EVENT_SHUTDOWN},
    {"log-massage", MPV_EVENT_LOG_MESSAGE},
    {"get-property-reply", MPV_EVENT_GET_PROPERTY_REPLY},
    {"set-property-reply", MPV_EVENT_SET_PROPERTY_REPLY},
    {"command-reply", MPV_EVENT_COMMAND_REPLY},
    {"start-file", MPV_EVENT_START_FILE},
    {"end-file", MPV_EVENT_END_FILE},
    {"file-loaded", MPV_EVENT_FILE_LOADED},
    {"tracks-changed", MPV_EVENT_TRACKS_CHANGED},
    {"tracks-switched", MPV_EVENT_TRACK_SWITCHED},
    {"idle", MPV_EVENT_IDLE},
    {"pause", MPV_EVENT_PAUSE},
    {"unpause", MPV_EVENT_UNPAUSE},
    {"tick", MPV_EVENT_TICK},
    {"script-input-dispatch", MPV_EVENT_SCRIPT_INPUT_DISPATCH},
    {"client-message", MPV_EVENT_CLIENT_MESSAGE},
    {"video-reconfig", MPV_EVENT_VIDEO_RECONFIG},
    {"audio-reconfig", MPV_EVENT_AUDIO_RECONFIG},
    {"metadata-update", MPV_EVENT_METADATA_UPDATE},
    {"seek", MPV_EVENT_SEEK},
    {"playback-restart", MPV_EVENT_PLAYBACK_RESTART},
    {"property-change", MPV_EVENT_PROPERTY_CHANGE},
    {"chapter-change", MPV_EVENT_CHAPTER_CHANGE},
    {"queue-overflow", MPV_EVENT_QUEUE_OVERFLOW}
  };

}