#include "include/headers.hpp"

namespace libmpv {

  //MPV COMMAND WRAPPERS//
  //for mpv_command and mpv_command_async
  Local<Value> Mpv::cmd_(const Nan::FunctionCallbackInfo<Value>& args, bool async=false) {
    Mpv *mpv = Nan::ObjectWrap::Unwrap<Mpv>(args.Holder());
    Result res;
    int err;
    mpv_handle *ctx = mpv->core.player_ready(args[0]);

    if (ctx) {
      Local<Array> arr = Nan::New<Array>().Cast(
        args[1 + (int)async]);; //convert args[1] to Local<Array> v8 values

      Cmdar cmd(arr);
      if(async) {
        In udata(args[1]);
        err = mpv_command_async(ctx, udata.toInt(), cmd.get());
      } else {
        err = mpv_command(ctx, cmd.get());
      }			
    } else {
      err = -3; //not ready ctx
    }
    res.addError(err);
    return res.getRes();
  }

  //for mpv_command_node and mpv_command_node_async
  Local<Value> Mpv::cmd_node_(const Nan::FunctionCallbackInfo<Value>& args, bool async=false) {
    Mpv *mpv = Nan::ObjectWrap::Unwrap<Mpv>(args.Holder());
    Result res;
    int err;
    mpv_handle *ctx = mpv->core.player_ready(args[0]);

    if (ctx) {
      int si = async;
      In nod(args[1 + si]); //make input node

      mpv_node* node = nod.toNode(); //get mpv_node from value
      mpv_node* outnode = new mpv_node; //create output node

      if(async) {
        In udata(args[1]); //get replay_user_data from v8 arg
        uint64_t reply_userdata = abs(udata.toInt());
        err = mpv_command_node_async(ctx, reply_userdata, node);
        res.addError(err);
      } else {
        err = mpv_command_node(ctx, node, outnode);
        res.addError(err);
        //check output data only when no error result and result not empty node
        if (err == 0 && outnode->format != MPV_FORMAT_NONE) {
          Out outval(outnode);
          res.addData(outval.value());
          Mpv::Mpv::log << "FREE API CREATED MPV NODE FORMAT : " << outnode->format << endl;
          mpv_free_node_contents(outnode);
        }
      }
    } else {
      err = -3; //not ready ctx
    }
    res.addError(err);
    return res.getRes();
  }

  //for mpv_get_property_string or mpv_get_property_osd_string as Local<Object>
  Local<Value> Mpv::get_property_str_(string entity, const Nan::FunctionCallbackInfo<Value>& args) {
    Mpv *mpv = Nan::ObjectWrap::Unwrap<Mpv>(args.Holder());
    Result res;
    int err;
    char *cmd_result_str = nullptr;
    mpv_handle *ctx = mpv->core.player_ready(args[0]);

    In prop(args[1]);
    if(ctx) {
      if(entity == "string") {
        cmd_result_str = mpv_get_property_string(ctx, prop.toStr());
        Mpv::log << "PropertyString cmd result : " << cmd_result_str << endl;
      } else if (entity == "osd-string") {
        cmd_result_str = mpv_get_property_osd_string(ctx, prop.toStr());
        Mpv::log << "PropertyOsdString cmd result : " << cmd_result_str << endl;
      }
      
      if (cmd_result_str == nullptr) {
        err = -8; //result is null -> property not found
      } else {
        err = 0; //OK result is in char*
        Out rval(cmd_result_str); //covert c-string to value
        res.addData(rval.value()); //add data
      }
    } else {
      err = -3;
    }

    Mpv::log << "FREE API CREATED STRING : " << cmd_result_str << endl;
    mpv_free(cmd_result_str);

    res.addError(err);
    return res.getRes();
  }

  //get mpv_get_property or mpv_get_property_async as Local<Object>
  Local<Value> Mpv::get_property_(const Nan::FunctionCallbackInfo<Value>& args, bool async=false) {
    Mpv *mpv = Nan::ObjectWrap::Unwrap<Mpv>(args.Holder());
    Result res;
    int err = -3; //error code for result
    mpv_handle *ctx = mpv->core.player_ready(args[0]);

    if (ctx) {
      In prop(args[1 + async]);
      In fmtval(args[2 + async]);
      Fmt<mpv_format> fmt(Maps::for_MAP, fmtval.toStr());
      mpv_format fmtf = (fmt.format() != MPV_FORMAT_NONE) ? fmt.format() : MPV_FORMAT_NODE;

      if (async) {
        In reply_udata(args[1]);
        err = mpv_get_property_async(ctx, abs(reply_udata.toInt()), prop.toStr(), fmtf);
      } else {
        void* data = Empty(fmtf).get();
        //void* data = nullptr;
        err = mpv_get_property(ctx, prop.toStr(), fmtf, data);

        //parse and add return data only if no error
        if (err == 0) {
          Local<Value> jsnode;
          Out rval;

          if (fmtf != MPV_FORMAT_NODE) {
            rval = Out(data, fmtf);
            jsnode = rval.value();
          } else {
            rval = Out(static_cast<mpv_node*>(data));
            jsnode = rval.value();
          }
          res.addData(jsnode); //add data to returned result
        }
        if(fmtf == MPV_FORMAT_STRING || fmtf == MPV_FORMAT_OSD_STRING) {
          char* apistr = *static_cast<char**>(data); //dereferenced void* pointer to char* data
          Mpv::log << "FREE API CREATED STRING : " << apistr << endl; 
          mpv_free(apistr);
        } else if(fmtf == MPV_FORMAT_NODE) {
          mpv_node* apinode = static_cast<mpv_node*>(data); //dereferenced void* pointer to mpv_node*
          Mpv::log << "FREE API CREATED MPV_NODE FORMAT : " << apinode->format << endl; 
          mpv_free_node_contents(apinode);
        }
      }
    } else {
      err = -3; //ctx not ready
    }
    res.addError(err);
    return res.getRes();
  }

  //set mpv_set_option, property, property_string, property_async as Local<Object>
  Local<Value> Mpv::set_(string entity, const Nan::FunctionCallbackInfo<Value>& args) {
    Mpv *mpv = Nan::ObjectWrap::Unwrap<Mpv>(args.Holder());
    map<string, int> entmap = {{"option", 1}, {"property", 1},
                               {"property-string", 1}, {"property-async", 2}};

    Result res;
    mpv_handle* ctx;
    int err = -3; //not ready
    ctx = mpv->core.player_ready(args[0], (entity == "option" ? false : true));

    if(ctx) {
      In opt_name(args[ 0 + entmap[entity] ]); //name of mpv option || property
      In val(args[ 1 + entmap[entity] ]); //value of mpv option || property

      if(entity == "property-string") {
        err = mpv_set_property_string(ctx, opt_name.toStr(), val.toStr());
      } else {
        Nan::Maybe<bool> asnod = Nan::To<bool>(args[2 + entmap[entity] ]);
        bool as_node = asnod.IsNothing() ? false : asnod.FromJust();

        mpv_format fmt; void* data;
        if (as_node) {
          fmt = MPV_FORMAT_NODE;
          data = static_cast<void*>(val.toNode());
          Mpv::log << "FORCE USE mpv_node for PRIMITIVE DATA TYPES" << endl;
        } else {
          fmt = val.toNode()->format;
          data = val.toData();
        }

        if(entity == "option") {
          err = mpv_set_option(ctx, opt_name.toStr(), fmt, data);
        } else if(entity == "property") {
          err = mpv_set_property(ctx, opt_name.toStr(), fmt, data);
        } else if(entity == "property-async") {
          In reudata(args[1]); //force parsed as INT64
          err = mpv_set_property_async(ctx, abs(reudata.toInt()), opt_name.toStr(), fmt , data);
        }

        Fmt<mpv_format> fmtname(Maps::for_MAP, fmt);
        Mpv::log << "SET " << (entity =="option" ? "OPTION" : "PROPERTY") << " USING FORMAT: "
          << fmtname.str() << endl;
      }
    } else {
      err = -3; //player not ready
    }
    res.addError(err);
    return res.getRes();
  }

} //namespace libmpv