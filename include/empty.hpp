#ifndef EMPTY_HPP
#define EMPTY_HPP

namespace libmpv {

  using namespace std;
  using namespace v8;

  template <class T>
    class Ep {
      public:
        Ep() : ep(makeEp()) {}
        void* get() { return ep; }

      private:
        void* ep;

        void* makeEp() {
          T* p = nullptr;
          void* eptr = &p;
          return eptr;
        }
    };

  class Empty {
    public:
      Empty(mpv_format fmt) : format(fmt), data(makeEmpty(format)) {}
      ~Empty() {}
      void *get() { return data; }

    private:
      mpv_format format;
      void *data;

      void* makeEmpty(mpv_format foemat) {
        void* ep = nullptr;
        if (format == MPV_FORMAT_STRING || format == MPV_FORMAT_OSD_STRING) {
          ep = Ep<char*>().get();
        } else if (format == MPV_FORMAT_FLAG) {
          ep = Ep<int>().get();
        } else if (format == MPV_FORMAT_INT64) {
          ep = Ep<int64_t>().get();
        } else if (format == MPV_FORMAT_DOUBLE) {
          ep = Ep<double>().get();
        } else if (format == MPV_FORMAT_NODE) {
          ep = Ep<mpv_node*>().get();
        }
        return ep;
      }

  };

} //namespace libmpv
#endif
