#ifndef MAPS_HPP
#define MAPS_HPP

namespace libmpv {
  
  struct Maps {
    static map<string, mpv_format> for_MAP;
    static map<string, mpv_end_file_reason> eof_MAP;
    static map<string, mpv_log_level> log_MAP;
    static map<string, mpv_event_id> evid_MAP;
    
    template <class T>
    static Local<Value> getMap(map<string, T> m);
  };
    
  
  template <class T>
  Local<Value> Maps::getMap(map<string, T> m) {
    Local<Object> mo = Nan::New<Object>();
    
    //TODO get map....... 
    typename map<string, T>::iterator it;
    for (it=m.begin(); it!=m.end(); ++it) {
      mo->Set(Nan::New<String>(it->first.c_str()).ToLocalChecked(),
        Nan::New<Int32>(static_cast<int32_t>(it->second)));
    }
    
    return mo;
  }

}
#endif