#ifndef IN_HPP
#define IN_HPP

namespace libmpv {

  class In {
    public:
      In();
      In(Local<Value> v8val); //automatic format detection from js
      ~In(); //run dealocator for whoole *mpv_node and delete this node

      mpv_node* toNode(); //get NODE
      char* toStr(); //get CSTRING
      int toFlag(); //get FLAG
      int64_t toInt(); //get INT64
      double toDouble(); //get DOUBLE
      void* toData(); //get node data void*

    private:
      Local<Array> seen;
      mpv_node* rnode; //output result in mpv_node* struct

      bool addToSeen(Local<Value> v8val); //add new v8 <Value> to seen
      int indexOfSeen(Local<Value> v8val); //check if is seen v8 <Value>

      mpv_format formatDetect(Local<Value> v8val); //automatic mpv_format detection
      char* valueToCstr(Local<Value> v8val); //convert v8val to char* c-string
      int valueToFlag(Local<Value> v8val); //convert v8val to int 0|1 flag
      int64_t valueToInt64(Local<Value> v8val); //convert v8val to int_64_t
      double valueToDouble(Local<Value> v8val); //convert v8val to double
      mpv_node* makeNode(Local<Value> v8val); ////convert v8val to mpv_node
      mpv_node_list* makeNodeList(Local<Value> v8val); //make mpv_node_list from v8value <Object> || <Array>
      mpv_byte_array* valueToByteArray(Local<Value> v8val); //convert v8val Buffer to mpv_byte_array

      void freeNode(mpv_node* nod); //recursive dealocate mpv_node
      void freeNodeList(mpv_node_list* nlst, mpv_format format); //run freeNode at every values and free mem
  };

}
#endif