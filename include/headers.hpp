#ifndef HEADERS_HPP
#define HEADERS_HPP

#include <iostream>
#include <iomanip>
#include <chrono>
#include <ctime>
#include <cstring>
#include <cmath>
#include <vector>
#include <map>
#include <streambuf>
#include <sstream>
#include <algorithm>

#include <nan.h>
#include <mpv/client.h>

//main Classes Core and Mpv
#include "loger.hpp" //method for logging
#include "core.hpp" //methods for handling mpv core and clients
#include "mpv.hpp" //main class for nodejs module

//other classes
#include "async.hpp" //wait for async events
#include "cmdar.hpp" //convert array <-> JS Array of strings
#include "evout.hpp" //event data parser
#include "empty.hpp" //create empty void* pointer for writing mpv_get
#include "fmt.hpp" //convert mpv_format <-> string
#include "in.hpp" //convert JS Value -> any mpv_format data or node
#include "maps.hpp" //map some enums to string values
#include "out.hpp" //convert any mpv data to JS Value
#include "result.hpp" //create JS object contain error and result

#endif