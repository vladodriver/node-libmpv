#ifndef MAIN_HPP
#define MAIN_HPP

#include "headers.hpp"

namespace libmpv {
  using namespace std;
  using namespace v8;

  /// MAIN MPV MODULE CLASS ///
  class Mpv : public Nan::ObjectWrap {
    public:
      static Log log;

      //register export all js class prototype methods
      static void Init(Handle<Object> exports);

    private:
      //Constructor/Destructor
      explicit Mpv(Core c);
      ~Mpv();

      Core core; //instance of Core class for create, initialize, detach, destroy ...
      static Nan::Persistent<Function> constructor; //Js constructor function -> New

      // JS constructor create Mpv module instance
      void static New(const Nan::FunctionCallbackInfo<Value>& args);
      static void mpv_status_getter(Local<String> prop, const Nan::PropertyCallbackInfo<Value>& args);
      static void mpv_enums_getter(Local<String> prop, const Nan::PropertyCallbackInfo<Value>& args);

      // MPV COMMAND WRAPPERS cmds.cpp // --> rewrite using classes Value, Parser, Events
      //void destroy_(const Nan::FunctionCallbackInfo<Value>& args, string mode);
      Local<Value> cmd_(const Nan::FunctionCallbackInfo<Value>& args, bool async);
      Local<Value> cmd_node_(const Nan::FunctionCallbackInfo<Value>& args, bool async);
      Local<Value> get_property_str_(string entity, const Nan::FunctionCallbackInfo<Value>& args);
      Local<Value> get_property_(const Nan::FunctionCallbackInfo<Value>& args, bool async);
      Local<Value> set_(string entity, const Nan::FunctionCallbackInfo<Value>& args);
      //void wait_event(const Nan::FunctionCallbackInfo<Value>& args);

      //STATIC METHODS exported to JS //
      static void debug(const Nan::FunctionCallbackInfo<Value>& args); //enable / disable debugging
      static void _test(const Nan::FunctionCallbackInfo<Value>& args); //only for testing

      // Js libmpv API //
      static void client_api_version(const Nan::FunctionCallbackInfo<Value>& args);
      static void error_string(const Nan::FunctionCallbackInfo<Value>& args);
      //mpv_free -> not possible implement -> is C specific
      //mpv_client_name -> included in Mpv.status property
      static void create(const Nan::FunctionCallbackInfo<Value>& args);
      static void initialize(const Nan::FunctionCallbackInfo<Value>& args);
      static void detach_destroy(const Nan::FunctionCallbackInfo<Value>& args);
      static void terminate_destroy(const Nan::FunctionCallbackInfo<Value>& args);
      static void create_client(const Nan::FunctionCallbackInfo<Value>& args);
      static void load_config_file(const Nan::FunctionCallbackInfo<Value>& args);
      static void suspend(const Nan::FunctionCallbackInfo<Value>& args);
      static void resume(const Nan::FunctionCallbackInfo<Value>& args);
      static void get_time_us(const Nan::FunctionCallbackInfo<Value>& args);
      //mpv_free_node_contents -> not possible implement -> is C specific
      static void set_option(const Nan::FunctionCallbackInfo<Value>& args);
      static void set_option_string(const Nan::FunctionCallbackInfo<Value>& args);
      static void command(const Nan::FunctionCallbackInfo<Value>& args);
      static void command_node(const Nan::FunctionCallbackInfo<Value>& args);
      static void command_string(const Nan::FunctionCallbackInfo<Value>& args);
      static void command_async(const Nan::FunctionCallbackInfo<Value>& args);
      static void command_node_async(const Nan::FunctionCallbackInfo<Value>& args);
      static void set_property(const Nan::FunctionCallbackInfo<Value>& args);
      static void set_property_string(const Nan::FunctionCallbackInfo<Value>& args);
      static void set_property_async(const Nan::FunctionCallbackInfo<Value>& args);
      static void get_property(const Nan::FunctionCallbackInfo<Value>& args);
      static void get_property_string(const Nan::FunctionCallbackInfo<Value>& args);
      static void get_property_osd_string(const Nan::FunctionCallbackInfo<Value>& args);
      static void get_property_async(const Nan::FunctionCallbackInfo<Value>& args);
      static void observe_property(const Nan::FunctionCallbackInfo<Value>& args);
      static void unobserve_property(const Nan::FunctionCallbackInfo<Value>& args);
      static void event_name(const Nan::FunctionCallbackInfo<Value>& args);
      static void request_event(const Nan::FunctionCallbackInfo<Value>& args);
      static void request_log_messages(const Nan::FunctionCallbackInfo<Value>& args);
      static void wait_event(const Nan::FunctionCallbackInfo<Value>& args);
      static void wakeup(const Nan::FunctionCallbackInfo<Value>& args);
      //mpv_set_wakeup_callback -> C specific and no practical usefull for JS
      static void get_wakeup_pipe(const Nan::FunctionCallbackInfo<Value>& args);
      static void wait_async_requests(const Nan::FunctionCallbackInfo<Value>& args);
      //mpv_get_sub_api -> C specific and no practical usefull for JS
  };

} //namespace libmpv
#endif
