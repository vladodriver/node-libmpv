#ifndef FMT_HPP
#define FMT_HPP

namespace libmpv {

  template<class T>
    class Fmt {
      public:
        Fmt(map<string, T> mp, T enm) : fmap(mp), fmt(enm) {
          fmtstr = setFmtstr(fmt);
        }
        Fmt(map<string, T> mp, string s) : fmap(mp), fmtstr(s) {
          fmt = setFmt(fmtstr);
        }
        Fmt(map<string, T> mp, const char* cs) : fmap(mp), fmtstr(string(cs)) {
          fmt = setFmt(fmtstr);
        }

        T format() { return fmt; }
        string str() { return fmtstr; }
        const char* cstr() { return fmtstr.c_str(); }

      private:
        map <string, T> fmap;
        T fmt;
        string fmtstr;

        T setFmt(string str) {
          T rfmt = T();
          if(fmap.find(str) != fmap.end()) {
            rfmt = fmap.find(str)->second;
          }
          return rfmt;
        }

        string setFmtstr(T ifmt) {
          string str = "none";            
          for (auto it=fmap.begin(); it != fmap.end(); ++it ) {
            if (it->second == ifmt) {
              str = it->first;
              break;
            }
          }
          return str;
        }
    };

}
#endif