#ifndef CORE_HPP
#define CORE_HPP

namespace libmpv {
  using namespace std;
  using namespace v8;

  /// Class handle mpv core main and client ctx create/destroy/init/status etc.. ///
  class Core {
    public:
      Core();
      ~Core();

      //public api cmds wrappers
      Local<Value> create(const Nan::FunctionCallbackInfo<Value>& args); //return status object;
      Local<Value> initialize(const Nan::FunctionCallbackInfo<Value>& args); //return Result object
      Local<Value> createClient(const Nan::FunctionCallbackInfo<Value>& args); //return Result object
      Local<Value> destroyHandle(const Nan::FunctionCallbackInfo<Value>& args, string mode); //return status object;

      //public methods
      Local<Value> status(); //js object info about player core and clients
      Local<Value> getMaps(); //js object info about mapped string values to enums
      mpv_handle* get_core(); //get mpv_handle* ctx or nullptr
      mpv_handle* player_ready(Local<Value> v8val, bool minit=true); //get core or client by name
      string main_ctx_name(); //get core ctx client name
      map<string, mpv_handle*> getClients(); //get all clients map
      bool getSuspended(); //get suspended state
      void setSuspended(bool sus); //set suspended state
    
    private:
      mpv_handle* main_ctx = nullptr;
      int core_status = -3; //uninitialized state
      string main_name;
      map <string, mpv_handle*> clients;
      bool core_suspended = false;	
  };

}
#endif