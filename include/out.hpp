#ifndef OUT_HPP
#define OUT_HPP

namespace libmpv {

  class Out {
    public:
      Out();
      Out(char* str); //Construct value from char*
      Out(mpv_node* node); //Construct value from mpv_node
      Out(void* data, mpv_format format); //Construct value from void* data and mpv_format

      ~Out();

      static void* getDataFromNode(mpv_node *node);
      Local<Value> value(); //get rval

    private:
      void* indata; 
      mpv_format fmt;
      Local<Value> rval;

      Local<Value> parseMpvNodeList(mpv_node_list *node_list);
      Local<Value> parseMpvNode(mpv_node* node);
      Local<Value> voidDataToValue(void* data, mpv_format format);
  };

}
#endif