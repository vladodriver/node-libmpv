#ifndef LOGER_HPP
#define LOGER_HPP

namespace libmpv {
  using namespace std;
  using namespace std::chrono;
  using namespace v8;

  //own output buffer abstract class derived from streambuf
  class outbuf : public streambuf {
    public:
      outbuf() {};
      ~outbuf() {};
    protected:
      // central output function
      virtual int_type overflow (int_type c) {
        if (c != EOF) {
          // and write the character to the standard output
          if (putchar(c) == EOF) {
            return EOF;
          }
        }
        return c;
      }
    };

  class Log : public outbuf {
    private:
      outbuf ob;
      ostream out;
      bool enabled = true;

      string timestamp() {
        high_resolution_clock::time_point p = high_resolution_clock::now();
        //milliseconds fs = duration_cast<milliseconds>(p.time_since_epoch());
        microseconds fs = duration_cast<microseconds>(p.time_since_epoch());
        seconds s = duration_cast<seconds>(fs);
        //size_t musec = fs.count() % 1000;
				size_t musec = fs.count() % 1000000;

        time_t tss = time(NULL);
        struct tm *tm = localtime(&tss);
       	//double sect = (static_cast<double>(mcsec)/1000) + tm->tm_sec;
				double sect = (static_cast<double>(musec)/1000000) + tm->tm_sec;
        stringstream ss;
        ss <<
          setw(2) << setfill('0') << tm->tm_year + 1900 << "-" << //full year
          setw(2) << setfill('0') << tm->tm_mon << "-" << //month
          setw(2) << setfill('0') << tm->tm_mday << "|" << //day in month
          setw(2) << setfill('0') << tm->tm_hour << ":" << //hour
          setw(2) << setfill('0') << tm->tm_min << ":" <<  //min
          fixed  << setw(6) << setfill('0') << setprecision(6) << sect; //sec with msec
        return ss.str();
      }

    public:
      Log() : ob(outbuf()), out(&ob) {}

      template<class T>
        ostream& operator<< (T val) {
        if(enabled) {	
          out << "[" << timestamp() << "] C++Log-> " << val;
        }
        return out;
      }

      bool on() {
        enabled = true;
        out.clear(); //clear old buffer
        out << "[" << timestamp() << "] C++Log-> " <<
          "DEBUG MODE NOW ENABLED!" << endl;
        return enabled;
      }

      bool off() {
        out << "[" << timestamp() << "] C++Log-> " <<
          "DEBUG MODE NOW DISABLED!" << endl;
        enabled = false;
        out.setstate(ios_base::eofbit);
        return enabled;
      }
  };
}
#endif