#ifndef RESULT_HPP
#define RESULT_HPP

namespace libmpv {

  class Result {
    private:
      int errcode;
      const char* error;
      Local<Object> res;

    public:
      Result() : res(Nan::New<Object>()) {} //insert empty js object
      ~Result() {}

      void addError(int err) {
        errcode = err;
        error = mpv_error_string(errcode);

        Local<Value> erval;
        if(errcode == 0) { //no error result
          erval = Nan::New<Boolean>(false);
        } else {
          erval = Nan::New<String>(error).ToLocalChecked();
        }
        res->Set(Nan::New<String>("error").ToLocalChecked(), erval);
        res->Set(Nan::New<String>("errorCode").ToLocalChecked(), Nan::New<Int32>(errcode));
      }

      void addData(Local<Value> val) {
        res->Set(Nan::New<String>("result").ToLocalChecked(), val);
      }

      Local<Value> getRes() { return res; }
  };

}
#endif