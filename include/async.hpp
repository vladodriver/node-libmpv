#ifndef ASYNC_HPP
#define ASYNC_HPP

#include "evout.hpp"

namespace libmpv {

  class Async : public Nan::AsyncWorker {
    public:
      Async(Nan::Callback *callback, mpv_handle* ctx, double to) :
      	AsyncWorker(callback), evctx(ctx), timeout(to) {}
      ~Async() {}

      void Execute () { //async wait for next mpv_event
        ev = mpv_wait_event(evctx, timeout);
      }

      void HandleOKCallback () { //run callback and pass to argv[]
        Evout evval(ev);

        Local<Value> argv[] = { evval.value() };
        callback->Call(1, argv, async_resource);
      }

    private:
      mpv_handle* evctx;
      double timeout;
      mpv_event* ev;
  };

}
#endif