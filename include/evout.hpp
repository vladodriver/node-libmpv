#ifndef DEVENT_HPP
#define DEVENT_HPP

namespace libmpv {

  /////////// Class Evout mpv_event -> to js object ////////// 
  class Evout {
    public:
      Evout(mpv_event* ev);
      ~Evout();

      Local<Value> value();

    private:
      mpv_event* event;
      Local<Value> eval;

      Local<Value> createEv(mpv_event* ev); //add basic filds to eval object
      Local<Value> addEvData(void* data, mpv_event_id evid); //parse and add event data to eval object
  };

  //// Class Evdata -> parser void* mpv_event->data ////
  class Evdata {
    public:
      Evdata(void * evd, mpv_event_id evid);
      ~Evdata();
      Local<Value> value();

    private:
      void* data;
      Local<Value> evout;

      Local<Value> makeEvdata(void* evd, mpv_event_id);
      Local<Value> parseEvData(mpv_event_property* evp);
      Local<Value> parseEvData(mpv_event_log_message* evlm);
      Local<Value> parseEvData(mpv_event_end_file* eveof);
      Local<Value> parseEvData(mpv_event_script_input_dispatch* evsid);
      Local<Value> parseEvData(mpv_event_client_message* evcm);	
  };

}
#endif