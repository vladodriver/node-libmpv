#ifndef ARRAY_HPP
#define ARRAY_HPP

namespace libmpv {

  //make const char** array from js Array [<String> values]
  class Cmdar {	
    public:
      Cmdar(Local<Value> val) : cmd(makeCmdar(val)) {}

      ~Cmdar() {
        int i = 0;
        while (cmd[i] != nullptr) {
          Mpv::Mpv::log << "FREE ALLOCATED STRING CMD: " << cmd[i] << endl;
          delete[] cmd[i];
          i++;
        }
        Mpv::Mpv::log << "FREE ALLOCATED CMD ARRAY LEN: " << i << endl;
        delete[] cmd;
      }

      const char** get() { return const_cast<const char**>(cmd); }

    private:
      char** cmd = nullptr; //array of converted type

      char** makeCmdar(Local<Value> v8val) {
        char** result = nullptr;
        if(v8val->IsArray()) {
          Local<Array> arr = v8val->ToObject().As<Array>();
          int arlen = arr->Length(); //number of converted strings
          result = new char*[arlen + 1]; //array of const char* -> length + NULL terminated

          int i;
          for (i = 0; i < arlen; i++) {
            Nan::Utf8String v8str(arr->Get(i));
            result[i] = new char[v8str.length() + 1];
            strcpy(result[i], *v8str);
          }
          result[i] = nullptr; //end of mpv_command -> NULL
        } else { //create only [NULL] array
          result = new char*[1];
          result[0] = nullptr;
        }
        return result;
      }
  };

  //make js Array of values from char ** c-string array
  class Arval {
    public:
      Arval(const char** cs, int len) : valar(makeArray(cs, len)) {}
      ~Arval() {}
      Local<Value> value() { return valar; }

    private:
      Local<Value> valar; //input v8 array

      Local<Value> makeArray(const char** csar, int len) {
        Local<Value> jsar = Nan::New<Array>();
        for(int i = 0; i < len; i++) {
          jsar->ToObject()->Set(
            Nan::New<Int32>(i),
            Nan::New<String>(csar[i]).ToLocalChecked()
          );
        }
        return jsar;
      }
    };

}
#endif